#!/usr/bin/env sh
set -eu

envsubst '${SERVER_HOST} ${SERVER_PORT}' < /etc/nginx/nginx.template > /etc/nginx/nginx.conf

exec "$@"
