# HTTPS proxy for tazendra API

Tazendra api -  http://tazendra.caltech.edu/~azurebrd/cgi-bin/forms/datatype_objects.cgi - is not served on https. This is a fix so [micropubs wormbase](https://gitlab.coko.foundation/micropubs/wormbase) can work with https. 

Checkout the `examples` folder for a working setup with  [docker-letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion) 
