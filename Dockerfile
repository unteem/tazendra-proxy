FROM nginx:1.17.0-alpine

COPY nginx.tmpl /etc/nginx/nginx.tmpl
COPY docker-entrypoint.sh /docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
